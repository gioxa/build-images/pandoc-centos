## purpose

"Pandoc is a Haskell library for converting from one markup format to another, and a command-line tool that uses this library."

This image adds pandoc to a micro CentOS 7 image with coreutils to convert Markdown to html.

**note:** no support for pdf yet!

## usage image

**example:**

```yaml
# .gitlab-ci.yml
build_docs:
  image: gioxa/pandoc-centos
  stage: build
  retry: 1
  dependencies: []
  variables:
    ODAGRUN_POD_SIZE: nano
  script:
    - mkdir -pv public
    - cp -v  Docs/app.css public/app.css
    - >-
      pandoc
      -s
      --highlight-style pygments
      --toc
      --toc-depth=2
      -c app.css
      -o public/index.html
      Docs/start_s.md
      Docs/intro.md
      Docs/install.md
  tags:
    - odagrun
  artifacts:
    paths:
      - public
```

## used by:

[odagrun](https://gitlab.com/gioxa/odagrun/odagrun) to publish html manual for each release, see result at [User Guide odagrun](https://downloads.odagrun.com/latest/files/manual/manual.html)
