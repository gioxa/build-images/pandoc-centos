# ${ODAGRUN_IMAGE_REFNAME}


[![](https://images.microbadger.com/badges/version/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/image/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/license/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME} "Get your own license badge on microbadger.com") [![](https://img.shields.io/badge/Gitlab-source-blue.svg)](${CI_PROJECT_URL})


## pandoc version

pandoc *v${version}*

---

${README_MD}

## make_os.conf

```bash

${MAKE_OS_CONF}

```

## Docker Image Config

```yaml

${DOCKER_CONFIG_YML}

```

---

*Build with [odagrun](https://www.odagrun.com) from GitLab Repo [$CI_PROJECT_PATH]($CI_PROJECT_URL) on openshift online starter.*
